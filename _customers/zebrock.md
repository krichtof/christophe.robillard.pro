---
layout: customer
name: Zebrock
contacts:
  - name: Edgard Garcia
    tel: 06 10 20 30 40

  - name: Helene Pons
    tel: 06 15 25 35 45

events:
  - name: Envoi facture 3287 (5 jours)
    date: 2017-08-01
    type: invoice

  - name: Proposer un parcours
    date: 2017-08-15
    type: merge-request
    url: https://gitlab/krichtof/zebrock/merge-request/2

  - name: Creer un parcours
    date: 2017-08-22
    type: merge-request
    url: https://gitlab/krichtof/zebrock/merge-request/3
---
